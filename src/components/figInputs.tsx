import React from 'react';
import { Typography, Slider, TextField, InputLabel, FormControl, Select, MenuItem, IconButton, Grid, Stack, FormGroup, FormControlLabel, Checkbox, Switch, Button } from '@mui/material';

import { Palette, Refresh } from '@mui/icons-material';
import { RgbaStringColorPicker as ColorPicker } from "react-colorful";


type SizeInputProps = {
  min?: number,
  max?: number,
  value: string | number,
  onChange: (value: string | number) => void,
}

type TextInputProps = {
  label: string
  value: string | number
  onChange: (value: string) => void
  type?: string
}

type NumberInputProps = {
  label: string
  value: number
  onChange: (value: number) => void
  step?: number
  width?: number | string
}

export const TextInput = ({ label, value, onChange, type }: TextInputProps) => <TextField
  label={label}
  value={value}
  size="small"
  type='number'
  variant="outlined"
  onChange={(event) => onChange(event.target.value)}
/>

export const NumberInput = ({ label, value, onChange, step, width }: NumberInputProps) => <TextField
  label={label}
  value={value}
  size="small"
  variant="outlined"
  type='number'
  onChange={(event) => onChange(parseFloat(event.target.value))}
  inputProps={{ step: step }}
  sx={{ width: width ? width : '100%' }}
/>

export const SizeInputs = ({ min = 25, max = 2500, value, onChange }: SizeInputProps) => {

  return (
    <Grid container alignItems='flex-end' >
      <Grid item xs={12} sm={4}>

        <TextInput
          label={'size'}
          value={value}
          onChange={onChange}
          type='number'
        />
      </Grid>


      <Grid item xs={12} sm={8} px={2}>
        <Slider
          value={typeof value == 'string' ? parseInt(value) : value}
          min={min}
          max={max}
          step={5}
          onChange={(event, value) => {
            if (typeof value == 'number') {
              onChange(value)
            }
          }}
        />
      </Grid>
    </Grid >
  )
}

type KeyInputProps = {
  label: string
  value: string
  onChange: (value: string) => void
}

export const KeyInput = ({ label, value, onChange }: KeyInputProps) => <TextField
  sx={{ width: "100%" }}
  label={label}
  value={value}
  size="small"
  variant="outlined"
  multiline={true}
  rows={3}
  key="keyInput"
  onChange={(event) => {
    console.debug('Input is', event.target.value)
    onChange(event.target.value);

  }}
/>

type FigTypeSelectProps = {
  value: string,
  showRefreshButton: boolean,
  onChange: (value: string) => void
}

export const FigTypeSelect = ({ value, onChange, showRefreshButton }: FigTypeSelectProps) => {
  return (
    <Grid
      container
      sx={{ width: "100%" }}
      alignItems="center"
    >

      <Grid xs={12} sm={10} item>
        <FormControl sx={{ width: "100%" }}>
          <InputLabel id="selectLabel">Figure type</InputLabel>
          <Select

            labelId="selectLabel"
            id="figTypeSelect"
            value={value}
            label="Figure type"
            onChange={(event) => onChange(event.target.value)}
          >
            <MenuItem value={'normal'}>Normal</MenuItem>
            <MenuItem value={'small'}>Small & many</MenuItem>
            <MenuItem value={'random'}>Random</MenuItem>
            <MenuItem value={'single'}>Single</MenuItem>
          </Select>
        </FormControl >
      </Grid>
      <Grid item xs={12} sm={2}>
        <IconButton>
          {showRefreshButton && <Refresh
            onClick={() => onChange(value)}
          />}
        </IconButton>
      </Grid>
    </Grid >
  )

}

type ColorProps = {
  color: string,
  onChange: (value: string) => void
  useRandomColors: boolean,
  setUseRandomColors: (value: boolean) => void
}

export const ColorInputs = (props: ColorProps) => {
  const { color, onChange, useRandomColors, setUseRandomColors } = props;
  const [showColorPalette, setShowColorPalette] = React.useState(false);

  return (
    <Stack
      width="100%"
      justifyContent="flex-start"
      alignItems="flex-start"

    >
      <Typography variant="overline">
        Colors
      </Typography>
      <Stack
        direction="row"
        justifyContent="space-between"
        width="100%"
      >
        <Button
          color={showColorPalette ? 'success' : 'secondary'}
          startIcon={<Palette />}
          onClick={() => setShowColorPalette((oldVal) => !oldVal)}
          aria-label="Pick color"
          title="Select a color for circles"
        >Select</Button>


        <FormControlLabel
          control={<Switch
            checked={useRandomColors}
            onChange={(event) => setUseRandomColors(event.target.checked)}
          />} label="Random" />

      </Stack>
      {showColorPalette && <ColorPicker
        color={color}
        onChange={onChange}
      />}
    </Stack>
  )
}