import React from 'react'
import { Box, Container, Stack, Divider, Grid, Paper } from '@mui/material'
import Fig from '../components/Fig';
import { green } from '@mui/material/colors';

//import Modal from 'react-modal'

import { FigSettings, DebugSettings } from '../tag/figTypes'
import { SizeInputs, KeyInput, NumberInput, FigTypeSelect, ColorInputs } from './figInputs';


//import logger from 'utils/logger'

const defaultFigSettings: FigSettings = {
  type: 'normal',
  size: 500,
  angle: 0,
  color: "rgba(255, 0, 0, 1)",
  //colorKey: ['black', 'green',],
  //satur: '80%',
  //light: '60%',
  border: 1
}

const defaultDebugSettings: DebugSettings = {
  logInfo: true,
  clearConsole: true,
  //  doChecks: false,
  //  stopAfter: 28,
  //  noOfLegs: 1,
  //  drawDebugInfo: true,
  //  drawControlKings: true,
  //  drawLegCenter: true,
}

type Props = {
  propName?: string
}

const showFig = true;
const menuSize = 3


const CuTagDraw: React.FC<Props> = (props: Props) => {


  const [figSettings, setFigSettings] = React.useState(defaultFigSettings);
  const [debugSettings, setDebugSettings] = React.useState(defaultDebugSettings);
  const [useDebugFig, setUseDebugFig] = React.useState(false);

  const changeFigSettings = (key: string, value: string | number | boolean) => {
    setFigSettings({ ...figSettings, [key]: value })
  }

  const showRefreshButton = figSettings.type == 'random' || figSettings.type == 'single';

  return (
    <Grid container wrap="nowrap">

      <Grid item
        xs={2} sm={3}
        minWidth={[100, 250]}
      >
        <Paper


          elevation={3}
          sx={{
            bgcolor: green[50],
            height: '100vh'
          }}

        >
          <Stack
            divider={<Divider flexItem />}
            spacing={2}
            justifyContent="flex-start"
            alignItems="flex-start"
            px={1}
            py={4}
          >


            <SizeInputs
              value={figSettings.size}
              onChange={(value) => changeFigSettings('size', value)}
            />


            <FigTypeSelect
              value={figSettings.type}
              onChange={(value) => changeFigSettings('type', value)}
              showRefreshButton={showRefreshButton}
            />



            {figSettings.type == 'normal' && <KeyInput
              label="Tag data (data to be held by figure)"
              value={figSettings.key}
              onChange={(value) => changeFigSettings('key', value)}
            />}


            {figSettings.type != 'single' && <NumberInput
              label={'Angle'}
              value={figSettings.angle || 0.0}
              onChange={(value) => changeFigSettings('angle', value)}
              step={0.1}
              width='75px'
            />}


            {figSettings.type != 'single' && <ColorInputs
              color={typeof figSettings.color == 'string' ? figSettings.color : 'red'}
              onChange={(value) => changeFigSettings('color', value)}
              useRandomColors={figSettings.useRandomColors}
              setUseRandomColors={(value) => changeFigSettings('useRandomColors', value)}
            />}

          </Stack>
          <Divider flexItem />
        </Paper>
      </Grid>

      <Grid item xs>

        {showFig && <Fig debug={useDebugFig} figSettings={figSettings} debugSettings={debugSettings} />}

      </Grid>

    </Grid>
  )

}

export default CuTagDraw