import React, {
  useCallback, useEffect, useRef, useState,
} from 'react';
import {
  Alert, AlertTitle, Button, Grid, Stack,
} from '@mui/material';
import fig from '../tag/fig';
import { FigSettings, DebugSettings } from '../tag/figTypes';

type Props = {

  debug: boolean,
  key?: string,
  figSettings?: FigSettings,
  debugSettings?: DebugSettings,
}

const Fig = ({ debug, figSettings, debugSettings }: Props) => {
  const canvasRef = useRef(null);
  const [errorMsg, setErrorMsg] = useState('');

  const drawFig = useCallback(() => {
    console.clear();
    try {
      if (debug) {
        console.info('Creating fig with debug functionality');
        fig.debugDraw(figSettings, debugSettings);
      } else {
        console.info('Creating fig');
        fig.draw(figSettings);
      }
      setErrorMsg('');
    } catch (err) {
      console.error(err.message);
      setErrorMsg(err.message);
    }
  }, [figSettings, debugSettings]);

  useEffect(() => {
    figSettings.canvas = canvasRef.current;
    drawFig();
  }, [drawFig]);

  const FigError = () => (
    <Stack spacing={2}>
      <Alert severity="error">
        <AlertTitle>Fig error</AlertTitle>
        {errorMsg}
      </Alert>
      <Button
        onClick={() => {
          setErrorMsg('');
          drawFig();
        }}
      >
        Try Again
      </Button>

    </Stack>
  );

  return (
    <Stack
      spacing={2}
      direction="column"
      alignItems="center"
      justifyContent="center"
      height="100vh"
    >

      {errorMsg && <FigError />}

      <canvas ref={canvasRef} />

    </Stack>

  );
};
export default Fig;
