/*

  Analyze and draw fig API - javascript

*/

import setupDebug from './createFigDebug/setupDebug';
import getFigKeyDebug from './key/getFigKeyDebug';
import getFigDataDebug from './createFigDebug/getFigDataDebug';
import createCirclesDebug from './createFigDebug/createCirclesDebug';
import drawDebug from './createFigDebug/drawDebug';

import drawSingle from './createFig/drawSingle';
import draw from './createFig/draw';
import getExampleFactors from './createFig/getFactorExamples';
import getDataFactors from './createFig/getDataFactors';

import { myTimer } from './createFig/utils';

import {
  DefaultFigSettings,
  DefaultDebugSettings,
  Pipe,
  MyApi,
} from './figTypes';

const onDrawArr: Function[] = [];

const defaultFigSettings: DefaultFigSettings = {
  type: 'normal',
  size: 500,
  light: '35%',
  satur: '100%',
  key: 'demoKey',
  angle: 0,
  color: 'red',
  border: undefined,
};

const defaultDebugSettings: DefaultDebugSettings = {
  clearConsole: false,
  logInfo: false,
  doChecks: false,
  drawDebugInfo: false,
  noOfLegs: 6,
  baseFactors: undefined,
  // freeze: undefined,
  stopAfter: undefined,
  // drawDebugInfoOnFig: false,
  // fakeCenter: false,
  kingControlThreshold: 0.0000001,
  drawControlKings: false,
  drawLegCenter: false,
  debugCircles: {
    light: '10%',
    satur: '100%',
    color: 150,
  },
  textColor: 'blue',
  logSettings: {
    log: {
      debug: true,
      info: true,
      warn: true,
      error: true,
    },
    expandLogGroup: {
      getFigKey: false,
      getFigData: false,
      createCircles: false,
      draw: false,
    },
  },
};

const myApi: MyApi = Object.seal({

  drawData: null,
  isDrawn: false,

  onDraw: (func, keepOld) => {
    if (func === false) {
      onDrawArr.length = 0;
      return;
    }
    if (!keepOld && onDrawArr.length) {
      // Reset onDraw
      onDrawArr.length = 0;
    } else if (typeof func === 'function') {
      onDrawArr.push(func);
    }
  },

  /// Draw the fig by piping some data through some functions
  debugDraw: (figSettings, debugSettings = {}) => {
    /// We check the input and add some data in the setup phase
    const pipeData = setupDebug({
      figSettings: { ...defaultFigSettings, ...figSettings },
      debugSettings: { ...defaultDebugSettings, ...debugSettings },
    });

    // We create a pipe function
    const pipe: Pipe = (...fns) => (x) => fns.reduce((y, f) => f(y), x);

    // We build the pipe
    const figFuncs = pipe(
      getFigKeyDebug,
      getFigDataDebug,
      createCirclesDebug,
      drawDebug,
    );

    // Here we create the figure by running the pipeData through the piped functions
    const drawData = figFuncs(pipeData);

    myApi.isDrawn = true;
    myApi.drawData = drawData;

    // If there has been assigned any callback functions, run these now with the draw data result
    onDrawArr.forEach((func) => func(drawData.output));

    return drawData.output;
  },

  draw: (figSettings) => {
    const {
      type, size, key, canvas,
    } = figSettings;
    const timer = myTimer(`Fig ${type}`);

    console.info('Drawing fig of type', type,
      'using settings', figSettings);
    timer.start();

    // We use a different simplified logic for the single type
    if (type == 'single') {
      drawSingle(size, canvas);
      timer.stop();
      return;
    }

    // If data, fetch data factors else fetch example factors based on type
    const factors = (type == 'normal' && key) ? getDataFactors(key) : getExampleFactors(type);

    draw(factors, figSettings);
    timer.stop();
  },

});

export default myApi;
