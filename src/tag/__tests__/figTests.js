
import fig from '../fig'

import { keyConstants } from '../constants'

const { KEY_LENGTH } = keyConstants

const settings = {
  size: 800,
  angle: 0.1,
  debug: false
}

const offsetSettings = {
  x: 20,
  y: 50,
  offsetSize: 900,
  key: 'Offset key'
}


expect.extend({
  toBeWithin(received, floor, ceiling) {
    const pass = !isNaN(received) && received >= floor && received <= ceiling
    if (pass) {
      return {
        message: () =>
          `expected ${received} not to be within range ${floor} - ${ceiling}`,
        pass: true,
      }
    } else {
      return {
        message: () =>
          `expected ${received} to be within range ${floor} - ${ceiling}`,
        pass: false,
      }
    }
  },
})

beforeAll(() => {


})

beforeEach(() => {

})


describe('Creating fig with no key', () => {

  test('Undefined key should throw', () => {

    expect(() => fig.draw()).toThrow()
    expect(() => fig.draw(1)).toThrow()

  })


})

describe('Creating fig with simple key', () => {

  doTests("0")

  doTests("abc")

  doTests("12345")

})


describe('Creating fig with advanced key', () => {

  const advancedKey = JSON.stringify({ test: 1, val: 1988, a: 'c' })

  doTests(advancedKey)

})


function doTests(key) {

  settings.key = key

  const result = fig.draw(settings)

  const data = fig.drawData

  test('figData', () => {

    expect(typeof data.figData).toBe('object')

    const { center, centerSize, circles, } = data.figData

    const centerPos = settings.size / 2
    expect(center).toHaveProperty('x', centerPos)
    expect(center).toHaveProperty('y', centerPos)

    expect(centerSize).not.toBe(NaN)

    const max = settings.size / 16

    circles.forEach(circle => {
      const { x, y, r } = circle
      expect(x).toBeWithin(0, settings.size)
      expect(y).toBeWithin(0, settings.size)


      expect(r).toBeWithin(0, max)

    })

  })


  test('Fig uri', () => {

    expect(typeof result.uri).toBe('string')
    expect(result.uri).not.toBe("")

  })


  test('Fig key', () => {

    expect(Array.isArray(data.figKey)).toBeTruthy()
    expect(data.figKey.length).toBe(KEY_LENGTH) //toBeWithin(0, 78)

  })

  test('Fig status', () => {

    const status = result.getStatus()
    expect(typeof (status)).toBe('string')
    expect(result.getStatus(true).length).toBeGreaterThan(1)
    expect(typeof (result.getStatus(0))).toBe('string')


  })


}


function doRedrawTest() {

  const res = fig.draw({ key: '12345', size: 600 })

  const orgFigUri = res.uri

  const newFig = fig.draw({ key: 'test' })

  test('Fig redraw', () => {


    expect(typeof newFig).toBe('object')

    expect(typeof newFig.uri).toBe('string')


    expect(newFig.uri !== '').toBeTruthy()

    expect(newFig.uri !== orgFigUri).toBeTruthy()
  })

}
