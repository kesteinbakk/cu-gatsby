import fig from '../fig'


//import constants from '../constants'

//const testKeyVersion = "1"



describe.skip('Testing analyze of simple key', () => {
  doTests('test')
})

describe.skip('Testing analyze of advanced key', () => {
  doTests('{d:1,a:b}')
})

function doTests(key) {

  const figUri = fig.draw(key).uri

  const analyzed = fig.analyze( figUri )

  expect ( typeof analyzed.data ).toBe('object')

  expect ( analyzed.key ).toBe(key)

  expect ( analyzed.uri ).toBeTruthy()

  expect ( typeof analyzed.uri ).toBe('string')

}
