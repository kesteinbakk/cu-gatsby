
import {
  quickSort,

/* logger*/
  getMedian,
  getAverage,
  getCenterFromPoints,
  getDistance,
  match
} from '../utils'

//const log = logger('UtilsTesting')




describe('Testing utils', ()=>{

  test('quickSort is working', ()=>{
    const arr = [5, 8, 10, 2, 1, 7, 3, 6, 4, 9, 0]
    const check = [...arr]

    quickSort(arr)
    check.sort( (a,b)=>a-b )

    //log.debug('Quicksort res & check', arr, check)
    expect(arr).toEqual(check)
  })

  test('quickSort with arr of objects is working', ()=>{
    const arrOfObj =[{v:5}, {v:8}, {v:10}, {v:2}, {v:1}, {v:7}, {v:3}, {v:0}, {v:6}, {v:4}, {v:9}, ]
    const check = [...arrOfObj]

    quickSort(arrOfObj, 'v')
    check.sort( (a,b)=>a.v-b.v )

    //log.debug('Quicksort obj res & check', arrOfObj, check)
    expect(arrOfObj).toEqual(check)
  })

  test('median', () => {
    expect ( getMedian( [ 2,45,80,1.5, 3, 0.8  ] ) ).toBe(2.5)
    expect ( getMedian( [1.33,2,50,300,40, 20, 70.321] ) ).toBe(40)
    expect ( getMedian( [{t:4}, {t:1}, {t:2}, {t:3}], 't' ) ).toBe(2.5)
    expect ( getMedian( [{val:100}, {val:20}, {val:3}, {val:40}, {val:5}], 'val' ) ).toBe(20)
  })

  test('average', () => {
    expect ( getAverage( [45,2,3,1.5] ) ).toBe(12.875)
    expect ( getAverage( [1.33,2,50,3,40] ) ).toBe(19.266)
    expect ( getAverage( [{t:1}, {t:2}, {t:3}, {t:4}], 't' ) ).toBe(2.5)
    expect ( getAverage( [{val:1}, {val:2}, {val:3}, {val:4}, {val:5}], 'val' ) ).toBe(3)
  })

  test('getCenter', () => {
    const p1 = {x:1, y:2}
    const p2 = {x:2, y:1}
    const p3 = {x:3, y:2}
    expect ( getCenterFromPoints(p1, p2, p3) ).toEqual({x:2, y:2})
  })

  test('getDistance', () => {
    const p1 = {x:0, y:0}
    const p2 = {x:10, y:10}

    expect ( getDistance(p1, p2) ).toBeCloseTo(14.1421)
  })
  
  test('match', () => {
    expect( match(0,0) ).toBeTruthy()
    expect( match( 1, 0) ).toBeFalsy()
    expect( match( 10, 9, 0.1) ).toBeFalsy()
    expect( match( 10, 9, 0.2) ).toBeTruthy()
  })
})
