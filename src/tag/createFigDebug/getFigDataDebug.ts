import {
  PipeData,
  CircleData,

} from '../figTypes'

import { checkHalfLegFactors } from './utils'

const me = 'getFigData'


const getFigData = (data: PipeData) => {

  const {
    config: {
      debugSettings: {
        logInfo,
        doChecks,
      },
      CONSTANTS: {
        FIG_CONSTANTS: { BASE_FACTORS, LEGS },
        KEY_CONSTANTS: {
          KEY_BASES,
          FACTOR_AREA
        },
      },
    },
    match,
    setup,
    keyData
  } = data

  const { log, myError, tearDown } = setup(me)

  if (!keyData) { throw myError('Missing keydata for calculating factors') }

  const {
    figKey,
    keyBase,
    figBase,
  } = keyData


  if (!(BASE_FACTORS.inner && BASE_FACTORS.outer && BASE_FACTORS.king)) {
    log.error('Base factors was', BASE_FACTORS)
    throw myError('Missing base factors. ')
  }

  if (!figKey) {
    log.error('Fig key missing. Key was:', figKey)
    throw myError('Missing figKey for fig gen. Typeof figKey was ' + typeof figKey)
  }

  const step = FACTOR_AREA / (2 * (figBase - 1))  // Half for key inc and half for next dec

  if (logInfo) {
    log.debug('Factor step is', step, '(Area available was', FACTOR_AREA, ')')
  }

  // The numeric values from the figKey is multiplied with a factor based on the
  // numeric base chosen for displaying the value in the fig
  const incKey = figKey.map(item => +item * step)

  const meta = getMeta(step)

  //data.figData.keyMeta = meta

  data.figData = {
    factors: getFactors({ meta, incKey }),
    circles: [] as CircleData[]
  }

  if (incKey.length) {

    log.error('Key', incKey)
    throw myError('I have leftovers of key (' + incKey.length + ')!')

  }

  tearDown()

  return data


  function getMeta(step: number) {

    // Length of array must equal KEY_META_LENGTH
    const keyMeta = [
      FACTOR_AREA, // Max offset. Start king. This number should be one above all the rest.
      step,
      KEY_BASES.indexOf(keyBase) * step,//kb[0], // keyBaseIndex1
      0, //Use color
      0, // Available
      0, // Available
    ]

    if (logInfo) {
      log.debug('META IS', keyMeta)
    }

    return keyMeta

  }



  function getLegPart(bases: number[], incKey: number[]): number[] {

    let offset = 0

    return bases.map((base, index) => {

      // Return factors
      if (index === bases.length - 1) {

        // Noe value on last circle. Used to clear offset for kings
        return base - offset

      }

      //Key value
      const inc = incKey.shift()


      /*       if (logCircleInfo) {
              log.debug('Inc for index', index, 'is', inc)
            } */

      // Circle size equals the base + keyInc - last offset
      const val = base + inc! - offset

      // Set new offset
      offset = inc!

      return val

    })


  }

  type GetFactorsArgs = { factors?: number[], meta: number[], incKey: number[] }
  type GetFactors = (args: GetFactorsArgs) => GetFactors | FigFactors

  function getFactors({ factors = [], meta, incKey }: GetFactorsArgs):
    GetFactors | FigFactors {

    const innerLeg = getLegPart(BASE_FACTORS.inner, incKey)
    const outerLeg = getLegPart(BASE_FACTORS.outer, incKey)

    if (doChecks) {
      checkHalfLegFactors(innerLeg)
      checkHalfLegFactors(outerLeg)
    }

    const kingInc = meta[factors.length]
    const halfKingInc = kingInc / 2
    const nextIndex = factors.length + 1 !== meta.length ? factors.length + 1 : 0
    const nextKingInc = meta[nextIndex]
    const halfNextKing = nextKingInc / 2

    const kingFactor = BASE_FACTORS.king + kingInc
    innerLeg[5] -= (halfKingInc)
    outerLeg[5] -= (halfKingInc + halfNextKing)

    const legFactors: LegFactors = [
      ...innerLeg,
      kingFactor,
      ...outerLeg
    ]

    if (doChecks) {
      checkLegFactors(legFactors, halfNextKing)
    }

    factors.push(legFactors)

    // If we haven't created all 6 legs, do it again
    if (factors.length < LEGS) {
      return getFactors({ factors, meta, incKey })
    }

    return factors as FigFactors
  }

}

export default getFigData