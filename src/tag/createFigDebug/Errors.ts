import { PipeData } from '../figTypes'

export class FigError extends Error {
  id?= 'unknown'
  data?: PipeData

  constructor(msg: string, id?: string, data?: PipeData) {
    super(msg)
    this.name = "FigError"
    if (id) { this.id = id }
    if (data) { this.data = data }
  }
}