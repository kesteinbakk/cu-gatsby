
/*

 Setup function - javascript

 Made by Karl Erik Steinbakk - kesteinbakk@gmail.com - 2016

*/

import { Input, PipeData, Phases, Info } from '../figTypes'
import * as CONSTANTS from '../constants'
import { FigError } from './Errors'
import { match } from './utils'
import logger from './logger'

const me = 'setup'
const log = logger(me)
const info: Info = {}
const status = ['No status set']
let ticks = 0
const { MAX_SIZE, MIN_SIZE } = CONSTANTS.FIG_CONSTANTS

export default function setup(input: Input): PipeData {

  const { figSettings, debugSettings } = input
  const { size } = figSettings
  const { logInfo, clearConsole } = debugSettings
  const canvas = checkCanvas(figSettings.canvas)

  if (clearConsole) { log.clear() }

  log.start(me, false)

  if (logInfo) {
    log.debug('Trying to draw fig with settings', figSettings)
    log.debug('Using debug settings', debugSettings)
  }

  if (size > MAX_SIZE || size < MIN_SIZE) {
    throw new FigError(`Fig size must be between ${MAX_SIZE} and ${MIN_SIZE}`)
  }

  const pipeData: PipeData = {

    /// Starting with the settings and some constants
    config: {
      figSettings,
      debugSettings,
      CONSTANTS
    },

    /// The canvas element where fig should be drawn
    canvas,

    /// Match matches two numbers with a threashold
    match,

    /// SetStatus stores strings for information about the fig generation
    setStatus: (str: string) => status.push(str),

    /// We then add a setup function which will be called by all functions in the pipe
    setup,

    /// We add a function for storing data
    setInfo,

    /// These functions are used to retreive certain parts of the data obj
    output: {

      // Status
      getStatus: (current?: boolean | number) => {

        // Return the last status msg if no argument
        if (current === undefined) { return status[status.length - 1] }

        // Return all status msgs if true
        if (current === true) { return status }

        // Return the specified if an index number is passed
        if (typeof current === 'number') { return status[current] }

        // Throw on other arguments
        log.error('Cannot pass argument ', current, 'to getStatus')
        throw Error('Invalid argument for getStatus')

      },

      // A getter for the info obj
      getInfo: () => info,

      // A getter for the number of ticks
      getTicks: () => ticks,

      // Timer
      time: Object.seal({
        start: +new Date(),
        end: null,
        used: ''
      }),
    }
  } // End of pipeData object


  function setup(id: Phases) {

    const { logSettings, logInfo } = debugSettings

    const localLog = logger(id, logSettings)

    if (logInfo) {
      localLog.start(id, logSettings.expandLogGroup[id] || false)
      localLog.debug('Starting', id)
    }

    const localMyError = (msg: string) => {
      const err = new FigError(msg, id, pipeData)
      const str = `Error from ${err.id}: ${err.message}`

      // Close active log group
      if (logInfo) {
        localLog.groupEnd()
      }

      localLog.error('ERROR: ' + str)
      return err // If used in a throw statement
    } // Local err func

    const localTearDown = () => {
      if (logInfo) {
        localLog.debug('Ending', id)
        localLog.end(id)
      }
    }

    const localStop = (reason: string = '') => {
      localLog.error('STOPPED: ', reason)
      throw localMyError('Stopped! ' + reason)
    }

    return {
      log: localLog,
      myError: localMyError,
      tearDown: localTearDown,
      stop: localStop,
    }

  } // End of setup function


  function setInfo(obj: Info | string, val?: any): void {
    if (typeof obj === 'string') {
      info[obj] = val
    } else if (typeof obj === 'object') {
      // Merge in new
      Object.keys(obj).forEach(key => info[key] = obj[key])
    } else {
      log.error('Invalid set info arguments', obj, val)
      throw Error('Invalid input for setInfo. Type was:' + typeof obj)
    }
  } // End of setInfo function

  log.end(me)

  return pipeData
}

/// Some support functions

// Checks that we have a valid canvas element
function checkCanvas(canvas: unknown) {
  let el: unknown

  if (typeof canvas === 'string') {
    el = document.getElementById(canvas)
  } else if (!canvas) {
    el = document.getElementsByTagName('canvas')[0] as HTMLCanvasElement
  } else {
    el = canvas
  }

  if (!(el && typeof el === 'object' && el instanceof HTMLCanvasElement)) {
    throw new FigError('Need canvas for fig generation')
  }

  return el
}

