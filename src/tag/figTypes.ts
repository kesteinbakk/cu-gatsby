import { LoggerApi, LoggerOptions } from './createFigDebug/logger'
import { FigError } from './createFigDebug/Errors'
import * as CONSTANTS from './constants'
import { KeyData } from './keyTypes'

/// Types for phases (file ids):

export type Phases =
  'getFigKey' | 'getFigData' | 'createCircles' | 'draw' | // Fig generation phases
  'readFigKey' // Fig analyze phases

/// Types for figure logic:




export type BaseFactors = {
  inner: number[],
  king: number,
  outer: number[]
}

export type MyFactors = {
  center: number,
  legs: number[][],
}

export type RadianFactors = {
  center: number,
  legs: number[][],
}

/// Types for settings/config: (figSettings, debugSettings and logSettings)

export type DefaultFigSettings = {
  canvas?: HTMLCanvasElement,
  type: 'normal' | 'random' | 'single' | 'small',
  size: number, // Size of canvas
  key: string,
  colorKey?: (number | string)[], // Either css color strings or 0-360 hsl hue
  light: string, // Lightness 0-100%, where 0% is black and 100% is all white. No effect if color is a string
  satur: string, // Saturation from 0-100%. 0 is no color (gray) while 100% is full color. No effect if color is a string
  angle: number, // In radian. An angle of PI turns the fig 180 degrees.
  color: string, // The default color if no colorKey. A css string
  useRandomColors?: boolean,
  fill?: true, // Fill the circles or just draw outline
  border?: number, // Number of pixels with border (all sides around fig)
}

export type FigSettings = Partial<DefaultFigSettings>

export type DefaultDebugSettings = {
  clearConsole: boolean,
  logInfo: boolean,
  doChecks: boolean,
  noOfLegs?: 1 | 2 | 3 | 4 | 5 | 6,
  baseFactors?: BaseFactors,
  stopAfter?: number,
  //freeze?: object,
  //maxTicks?: number,
  //fakeCenter?: object | boolean,
  kingControlThreshold: number,
  drawControlKings: boolean,
  drawDebugInfo: boolean,
  drawLegCenter: boolean,
  debugCircles: {
    light: string,
    satur: string,
    color: number | string
  },
  textColor: string,
  logSettings: LoggerOptions & {
    expandLogGroup: { [key in Phases]?: boolean }
  }
}

export type DebugSettings = Partial<DefaultDebugSettings>

export type Settings = {
  figSettings: Partial<DefaultFigSettings>,
  debugSettings?: Partial<DefaultDebugSettings>
}

export type Input = { figSettings: DefaultFigSettings, debugSettings: DefaultDebugSettings }


/// Types for support functions
export type Match = (num1: number, num2: number, threshold?: number, base?: number) => boolean | number

type PipeInputFunction = (data: PipeData) => PipeData
type PipeStartFunction = (x: PipeData) => PipeData
export type Pipe = (...fns: PipeInputFunction[]) => PipeStartFunction


type Setup = (id: Phases, expand?: boolean) => {
  log: LoggerApi
  tearDown: () => void,
  myError: (msg: string) => FigError,
  stop: () => never
}


/// Types for data:

export type Config = {
  figSettings: DefaultFigSettings & { key: string },
  debugSettings: DefaultDebugSettings,
  CONSTANTS: typeof CONSTANTS
}

export type CircleData = {
  x: number,
  y: number,
  radius: number,
}

export type CircleDataDebug = {
  no: number,
  x: number,
  y: number,
  radius: number,
  R: number,
  angle: number
}

type FigData = {
  factors?: RadianFactors
  circles: CircleData[],
}

export type DebugData = {
  circles: CircleDataDebug[],
  kings: { x: number, y: number, radius: number, factor: number }[],
  legEndInfo: { legCenterX: number, legCenterY: number, legAngle: number, circle?: CircleData }[],
  controlKings: CircleData[],
  bigCircleCenters: CircleData[],
  steps: number
}

export type Info = { [key: string]: any }

export type OutputData = {
  getStatus(): string | string[],
  getInfo(): Info,
  getTicks(): number,
  time: {
    start: number,
    end: number | null,
    used: string
  }
  key?: string
  coreKey?: string
  uri?: string,
}

export type PipeData = {
  config: Config,
  canvas: HTMLCanvasElement,
  match: Match
  setup: Setup,
  setStatus: (str: string) => void,
  setInfo: (obj: Info | string, val?: any) => void
  keyData?: KeyData, //Added in writeFigkey
  figData?: FigData, // Added by create/init
  debugData?: DebugData, // Will only be added in pipe if debug is true
  output: OutputData
}


/// Main fig api


type CallbackOnDraw = (data: OutputData) => void

export type MyApi = {
  debugDraw: (figSettings: FigSettings, debugSettings?: DebugSettings) => PipeData['output'],
  draw: (figSettings: FigSettings) => void,
  onDraw: (func: boolean | CallbackOnDraw, keepOld: boolean) => void
  isDrawn: boolean,
  drawData: PipeData | null,
}
