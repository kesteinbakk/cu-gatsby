

const me = 'getMetaData'


const getMetaData = (data) => {

  const {
    center,
    setInfo,
  } = data

  const {
    debug,
    maxTicks,
    constants: { PIx2, PId3, THE_MEVEM_VALUE, }
  } = data.input

  const { log, myError, tearDown } = data.setup(me)

  const bigSize = data.center.radius / THE_MEVEM_VALUE

  const figSize = bigSize / THE_MEVEM_VALUE

  setInfo({
    'Analyzed fig size': Math.round(figSize),
  })

  const kings = []

  const maxAngle = PIx2 + center.angle

  let angle = 0

  while (angle < maxAngle) {

    if (debug) {

      if (++data.output.usedTicks === maxTicks) {

        throw myError('Max number of ticks reached')

      }

      log.info('Fig data tick no', data.output.usedTicks)

    } // debug

    angle += PId3

  }

  tearDown()

  return kings

}

export default getMetaData
