

const me = 'getKeyFromFactors'


const getKeyFromFactors = (data) => {


  const {

    setInfo,

  } = data


  const {

    debug,
    baseFactors,
    keyConstants: {
      KEY_BASES,
      BASE_FACTORS,
      KEY_LENGTH,
      KEY_META_LENGTH
    },

  } = data.input


  const factors = data.factors || data.input.factors // (from input if key is used direct)


  const { log, myError, tearDown } = data.setup(me)

  const bases = baseFactors || BASE_FACTORS


  const sortLegFactors = (factors, startIndex) => {

    if (!startIndex) {

      return factors

    }

    const sorted = []

    while (sorted.length < factors.length) {

      sorted.push(factors[startIndex])

      if (++startIndex === factors.length) {

        startIndex = 0

      }

    }

    if (debug) {

      log.debug('Sorted figFactors', sorted)

    }

    return sorted

  }


  const getIncs = (factors, bases, offset) => {


    const getInc = (factor, base, offset) => {

      const inc = factor - base + offset

      return inc < 0 ? 0 : inc

    }


    return factors.reduce((acc, legFactors) => {

      legFactors.reduce((offset, factor, index) => {

        if (index === 6) {

          acc.meta.push(getInc(factor, bases.king, 0))

          return 0

        }

        const base = index < 6 ? bases.inner[index] : bases.outer[index - 7]

        const inc = getInc(factor, base, offset)

        acc.key.push(inc)

        return inc

      }, 0)

      return acc

    }, { key: [], meta: [] })

  }


  /*LOGIC*/


  if (!Array.isArray(factors) || factors.length !== 6) {

    log.error('Invalid fig factors', factors)
    throw myError('Missing or invalid figFactors. Connot get key', data)

  }


  const startIndex = factors.reduce((acc, leg, index, arr) =>
    leg[6] > arr[acc][6] ? index : acc, 0)


  if (debug) {

    log.debug('Start index is', startIndex)
    log.debug('Input factors', [...factors])

  }

  const sortedFactors = sortLegFactors(factors, startIndex)

  if (debug) {

    log.debug('Sorted factors', sortedFactors)

  }

  const incs = getIncs(sortedFactors, bases)

  if (incs.meta.length !== KEY_META_LENGTH) {

    throw myError('Invalid meta length')

  }
  const [
    area,
    step,
    ...restMeta
  ] = incs.meta

  const figBase = Math.round(area / (step * 2)) + 1

  setInfo('figBase', figBase)

  const [
    keyBaseIndex,
    useColor,
    figVersion,
    check,
  ] = restMeta.map(factor => Math.round(factor / step))


  if (check !== 0) {

    log.error('Meta factors was', incs.meta)

    throw myError('Invalid zero check. Check was' + check)

  }


  const keyBase = KEY_BASES[keyBaseIndex]

  setInfo('Keybase', keyBase)


  const figKey = incs.key.map(inc => Math.round(inc / step))

  if (debug) {

    log.debug('Figkey:', figKey)

  }

  if (figKey.length !== KEY_LENGTH) {

    throw myError('Invalid figKey length. Fig key length was: ' + figKey.length)

  }
  data.figKey = figKey

  data.meta = {
    figBase,
    keyBase,
    useColor,
    figVersion,
  }

  tearDown()

  return data

}

export default getKeyFromFactors