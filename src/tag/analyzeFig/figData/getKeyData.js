import getCircleData from '../circleData/getCircleData'

//import getNext from './_getNext'

const me = 'getKeyData'


const getKeyData = (data) => {

  const {

    center,
    bigSize,
    //  figSize,
    setInfo,
    setStatus,

    utils: { getOffsetPoint },
    markFig: { clearMarks, mark },

    isHit

  } = data


  const {
    [me]: {
      //maxNoOfSteps,
      //clearMarksOnStep,
      radiusIncFactor,
      radiusDecFactor,
      estimatedFirstCircle
    },
    debug,
    maxTicks,
    constants: { PI, PId3, THE_MEVEM_VALUE, }
  } = data.input


  const { log, myError, tearDown } = data.setup(me)


  const firstInc = (1 + estimatedFirstCircle) * THE_MEVEM_VALUE
  // Center (1) plus estimated first circle (0.4)

  //  let circleNum


  const getLegFactors = (angle = 0, leg = 0, factors = []) => {

    setStatus('Analyzing leg no ' + leg + 1)

    factors[leg] = []

    const anglePI = angle - PI

    const legData = {

      x: center.x + (bigSize * Math.cos(anglePI)),
      y: center.y + (bigSize * Math.sin(anglePI)),

      angle: angle + firstInc,


      factors: factors[leg]

    }

    getCircleFactors(legData)

    while (factors.length < 6) {

      getLegFactors(angle + PId3, ++leg, factors)

    }

    return factors

  }


  const getCircleFactors = (legData) => {

    const { x, y, angle, factors, inc = true } = legData


    if (debug) {

      if (++data.output.usedTicks === maxTicks) {

        throw myError('Max number of ticks reached')

      }

      log.info('Fig data tick no', data.output.usedTicks)

    } // debug


    const offsetPoint = getOffsetPoint({ x, y, angle }, bigSize)

    if (debug) {

      //log.debug('Offset point is', point)
      mark({ ...offsetPoint, type: 'offset' })

    }

    if (!isHit(offsetPoint)) {

      log.error('No hit. Leg data was', legData)
      throw myError('Offset point is not a hit!')

    }

    const circle = getCircleData(data, offsetPoint)


    const factor = circle.radius / center.radius

    if (isNaN(factor)) {

      log.error('NAN Factor. Leg data was', legData)

      throw myError('Factor is NAN')

    }

    factors.push(factor)

    if (factors.length === 6) {

      legData.inc = false

    }

    if (factors.length === 13) {

      return true

    }

    // Push big curve ahead 2nd radius for this circle and 1st for next
    const thisAngleInc = factor * THE_MEVEM_VALUE

    const nextInc = inc ? radiusIncFactor : radiusDecFactor
    const nextAngleInc = factor * nextInc * THE_MEVEM_VALUE

    legData.angle += thisAngleInc + nextAngleInc

    return getCircleFactors(legData)

  }

  clearMarks()

  const factors = getLegFactors( /*angle*/)

  setInfo({

    'NoOfLegs': factors.length
  })

  tearDown()

  return factors

}

export default getKeyData
