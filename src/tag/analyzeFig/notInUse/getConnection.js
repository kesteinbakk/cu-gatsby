

/*

 getconnection  - javascript

 Returns arr of connection


*/



import getCirclePaths from './getCirclePaths'

const me = 'getConnection'



export default(data, startPos) => {

  const {
    debug,
    [me]: {
      noOfChecks,
      acceptedConnectionSize
    },
    constants: { PIx2 },
    
  } = data.input
  


  const { x, y, radius } = startPos

  const {myError, log, tearDown } = data.setup(me)


  const inc = PIx2 / noOfChecks

  const acceptedSize = acceptedConnectionSize / radius

  let counter = 0

  do {


    const connection = getCirclePaths( data, {
      x,
      y,
      angle: counter * inc,
      noOfPaths: 1,
      startAtSize: radius * 2,
    })[0]

    if ( debug ) {
      log.debug('Got possible connection', connection)
      log.debug('Size limit is', acceptedSize)
    }

    if ( connection.size > acceptedSize ) {

      if ( debug ) {
        log.debug('Got connection', connection)

      }

      tearDown()

      return connection.angle

    }

  } while ( ++counter <= noOfChecks)

  throw myError('Failed getting connection angle. ')

}
