

export const timer = (initName: string) => {

  type MyTimer = {
    startTime: number,
    name: string,
    start: (name: string) => void,
    stop: (name: string) => void
  }

  const myTimer: MyTimer = {
    startTime: 0,
    name: initName || 'noName',

    start: (name) => {

      myTimer.name = name || myTimer.name
      myTimer.startTime = Date.now()
      console.log('Starting', myTimer.name)

    },

    stop: (endMsg) => {

      console.log('Ending', myTimer.name, 'Used', Date.now() - myTimer.startTime, endMsg || '')

    }

  }
  return myTimer

}

export const averageOfArr = (arr: number[]) => arr.reduce((acc, cur) => acc + cur, 0) / arr.length

// MatchAll
// Returns true if all sets of numers match.
// Returns total diff if within given threshold parameter
// Returns false if no match
export const matchAll = ({ numbers, threshold, base }) => {

  return numbers.reduce((red, currentNumbers) => {

    if (red === false) {

      return false

    }
    const { num1, num2 } = currentNumbers
    const res = match({ num1, num2, threshold, base })
    if (res === false) {

      return false

    }
    if (res === true) {

      return red

    } else {

      return red === true ? res : red + res

    }

  }, true)

}

// Match numbers
// Returns true if exact match.
// Returns num dif if within threshold provided
// Returns false if no match
// Matches two numbers. Returns true if exact match, the difference if within
// the threshold, and false if outside the threshold
export const match = (num1: number, num2: number, threshold?: number, base?: number) => {

  if (num1 === num2) { return true }

  threshold = threshold || 0.00001
  base = base || Math.max(num1, num2)

  const offset = threshold * base
  if (num1 < (num2 + offset) && num1 > (num2 - offset)) {
    return Math.abs(num2 - num1)
  } else {
    return false
  }

}

export const loop = (num, func, res) => {

  for (let i = 0; i < num; i++) {

    res = func(res, i)

  }
  return res

}

export const getRGB = ({ x, y, image }) => {

  const pixel = (image.size * Math.round(y)) + Math.round(x)
  const pix = pixel * 4
  const data = image.data

  return {
    r: data[pix],
    g: data[pix + 1],
    b: data[pix + 2],
    a: data[pix + 3]
  }

}

export const checkRGB = ({ r, g, b, threshold = 20 }) => {

  return r >= threshold || g >= threshold || b >= threshold

}

// Returns true if color, false if black/white/grayscale
export const isColor = ({ r, g, b, threshold }) => {
  if (match(r, g, threshold, 150) && match(r, b, threshold, 150)) { //Is grayscale
    return false
  }
  return true
}

// Trampoline that prevents stack overflow
// Func must return a self as a function
// Used: res = trampoline( recFunc )(input)
export const trampoline = fn => (...args) => {

  let result = fn(...args)

  while (typeof result === 'function') {

    result = result()

  }

  return result

}

export const isNumeric = (value, notZero) => {

  if (value.length) {

    let test = true
    value.forEach(val => {

      if (isNaN(val - parseFloat(val))) {

        test = false

      }
      if (notZero && val === 0) {

        test = false

      }

    })
    return test

  }

  if (notZero && value === 0) {

    return false

  }

  return !isNaN(value - parseFloat(value))

}

/*export const  valueIs = type=>value=> {
  return typeof value === type;
}*/

export const compose = (...fns) => x => fns.reduceRight((v, f) => f(v), x)

export const asyncCompose = (...fns) => input =>
  fns.reduceRight((chain, func) => {

    if (typeof func !== 'function') throw Error('Invalid function passed to composer!')
    return chain.then(func).catch(err => {

      throw err

    })

  },
    Promise.resolve(input)
  )


export const pipe = (...fns) => x => fns.reduce((y, f) => f(y), x)

export const asyncPipe = (...fns) => input =>
  fns.reduce((chain, func) => chain.then(func).catch(err => {

    console.error('Async pipe failed:', func.name, err)
    //throw err

  }), Promise.resolve(input))

export const pipeDebug = (...fns) => x => fns.reduce((y, f) => {

  console.debug('PIPE: Received obj', y)
  console.debug('PIPE Func is', f)
  return f(y)

}, x)


//First letter in string to to upper case
export const firstUpper = (str) => {

  if (!str || !isNaN(str)) {

    return str

  }

  return str[0].toUpperCase() + str.slice(1)

}

export const noSignMod = (a, n) => a - Math.floor(a / n) * n

export const angleDif = (a1, a2) => {


  const PIx2 = Math.PI * 2

  while (a1 > PIx2) {

    a1 -= PIx2

  }
  while (a1 < 0) {

    a1 += PIx2

  }

  while (a2 > PIx2) {

    a2 -= PIx2

  }

  while (a2 < 0) {

    a2 += PIx2

  }

  console.debug('Comparing', a1, a2)
  return Math.min(PIx2 - Math.abs(a1 - a2), Math.abs(a1 - a2))

}

export const getDistance = (p1, p2) => {

  const { x: x1, y: y1 } = p1
  const { x: x2, y: y2 } = p2
  return Math.hypot(x2 - x1, y2 - y1)

}

export const getDistanceAlternative = (p1, p2) => {

  // A bit slower than main getDistance
  const { x: x1, y: y1 } = p1
  const { x: x2, y: y2 } = p2
  return Math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)

}

export const getSlope = (p1, p2) => {

  const { x: x1, y: y1 } = p1
  const { x: x2, y: y2 } = p2
  return (y1 - y2) / (x1 - x2)

}

export const getAngleFromTwoPoints = (p1, p2) => {

  return Math.atan2(p2.y - p1.y, p2.x - p1.x)

}

export const getAngleFromTwoLines = (m1, m2) => {

  const x = (m2 - m1) / (1 + m2 * m1)
  return Math.atan(x)

}


// Middle value of two points on a line
export const getMiddle = (x, y) => Math.round(Math.abs(x + (y - x) / 2))

// Get the sum of difference between the numbers in an array (of objects if key)
export const getDif = (arr, key) => arr.reduce((acc, val, index, me) => {

  if (index === 1) {

    acc = 0

  }

  if (key) {

    val = val[key]

  }

  const prev = key ? me[index - 1][key] : me[index - 1]

  const diff = Math.abs(prev - val)

  return acc + diff

})


export const shuffleArray = (array) => {

  let currentIndex = array.length, temporaryValue, randomIndex

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex)
    currentIndex--

    // And swap it with the current element.
    temporaryValue = array[currentIndex]
    array[currentIndex] = array[randomIndex]
    array[randomIndex] = temporaryValue

  }

  return array

}

//Gets max value of an array (of objects if key)
export const getMaxValue = (arr, key) => arr.reduce((acc, val, index) => {

  if (key) {

    if (index === 1) {

      acc = acc[key]

    }

    val = val[key]

  }

  if (acc < val) {

    return val

  }

  return acc

})

//Gets min value of an array (of objects if key)
export const getMinValue = (arr, key) => arr.reduce((acc, val, index) => {

  if (key) {

    if (index === 1) {

      acc = acc[key]

    }

    val = val[key]

  }

  if (acc > val) {

    return val

  }

  return acc

})

export const getMin = (arr, key, minLevel) => arr.reduce((acc, current, index) => {


  if (minLevel && current[key] < minLevel) {

    return acc

  }

  if (acc[key] > current[key]) {

    return current

  }

  return acc

})

export const getMax = (arr, key) => arr.reduce((acc, current, index) => {

  if (acc[key] < current[key]) {

    return current

  }

  return acc

})

//Gets avg value of an array (of objects if key)
export const getAverage = (arr, key) => arr.reduce((acc, val, index) => {

  if (key) {

    if (index === 1) {

      acc = acc[key]

    }

    val = val[key]

  }

  return acc + val

}) / arr.length


export const getMedian = function (array, key) {

  if (array.length === 0) {

    // Allow this by returning 0?
    throw new Error('Trying to get median with array length 0.')

  }

  // If only 1 item return this item
  if (array.length === 1) {

    return key ? array[0][key] : array[0]

  }


  // Make copy as quickSort will change array
  const arr = [...array]
  // Sort copy

  quickSort(arr, key)

  // Get middle
  const mid = Math.floor(arr.length / 2)
  const notEven = arr.length % 2

  if (notEven) {

    return key ? arr[mid][key] : arr[mid]

  }
  // Even length:
  if (key) {

    return (arr[mid - 1][key] + arr[mid][key]) / 2

  }
  return (arr[mid - 1] + arr[mid]) / 2

}


export const getOffsetPoint = ({ x, y, angle }, offset) => ({
  x: x + (offset * Math.cos(angle)),
  y: y + (offset * Math.sin(angle)),
  angle
})


export const getCenterFromPoints = (p1, p2, p3) => {

  // Thanks Paul Bourke > http://paulbourke.net/geometry/circlesphere/

  const x1 = p1.x, x2 = p2.x, x3 = p3.x
  const y1 = p1.y, y2 = p2.y, y3 = p3.y
  /*	if ( isNaN(x1+x2+x3+y1+y2+y3) ) {
      throw new Error('Invalid inputs for getCenter')
    }*/
  const ma = (y2 - y1) / (x2 - x1)
  const mb = (y3 - y2) / (x3 - x2)

  const x = ((ma * mb * (y1 - y3)) + (mb * (x1 + x2)) - (ma * (x2 + x3))) /
    (2 * (mb - ma))
  const y = -(1 / ma) * (x - (x1 + x2) / 2) + (y1 + y2) / 2

  return { x, y }

}

/*export const checkObj = ( { obj, deep, check, res = {
      success : true,
      msg : "",
      key : [],
      check : []
    } } ) => {


  Object.keys(obj).forEach( (key) => {

    if ( ! res.check.length ) {
      res.check.push(key);
    }
    res.check[ res.check.length-1] = key;

    if ( deep && typeof obj[key] === "object") {
      res.check.push(key)
      return checkObj( {obj: obj[key], deep:deep, check:check, res:res} );
    }

    if ( ! check(obj[key]) ) {
      res.success = false;
      res.key.push(res.check.join(" > "))
    }

  })

  res.check.pop()

  return res;

}*/

export const calcLine = ({ x1 = 0, y1 = 0, x2, y2, step = 1 }) => {

  const coords = []

  if (isNaN(x1 + x2 + y1 + y2)) {

    console.error('Invalid pixels', x1, x2, y1, y2)

    const err = new Error('Invalid pix for line calc')
    err.type = 'supportError'
    throw err

  }
  // Define differences and error check
  const dx = Math.abs(x2 - x1),
    dy = Math.abs(y2 - y1),
    sx = (x1 < x2) ? 1 : -1,
    sy = (y1 < y2) ? 1 : -1


  let err = dx - dy,
    counter = 0,
    e2

  // Set first coordinates
  //coords.push(y1, x1));
  // Main loop
  while (!(x1 === x2 && y1 === y2)) {

    e2 = err << 1
    if (e2 > -dy) {

      err -= dy
      x1 += sx

    }
    if (e2 < dx) {

      err += dx
      y1 += sy

    }
    // Set coordinates
    if (++counter % step === 0) {

      coords.push({ x: x1, y: y1 })

    }

    if (counter > 100000) {

      const err = new Error('Neverending loop')
      err.type = 'supportError'
      throw err

    }

  }
  // Return the result
  return coords

}


export const getRandomInt = (min, max) => {

  min = Math.ceil(min)
  max = Math.floor(max)
  return Math.floor(Math.random() * (max - min)) + min //The maximum is exclusive and the minimum is inclusive

}

export const getRandom = (min, max) => {

  return Math.random() * (max - min) + min //The maximum is exclusive and the minimum is inclusive

}

export const round = (value, precision) => {

  const multiplier = Math.pow(10, precision || 0)
  return Math.round(value * multiplier) / multiplier

}

// Set as default property value if parameter is required
export const reqPara = (name) => {

  throw new Error(`Parameter ${name} is required!`)

}


export const quickSort = (arr, key) => {

  if (key) {

    return quickSortObj(arr, key)

  } else {

    return quickSortPure(arr)

  }

}


export const getPixelFromCoord = ({ imgData, width, height, x, y }) => {

  return (y * width) + x

}

export const lineIntersect = ({ x1, y1, x2, y2, x3, y3, x4, y4 }) => {

  // Thank you Paul Bourke and vbarbarosh
  // https://stackoverflow.com/questions/13937782/calculating-the-point-of-intersection-of-two-lines
  //http://paulbourke.net/geometry/pointlineplane/javascript.txt

  if ((x1 === x2 && y1 === y2) || (x3 === x4 && y3 === y4)) {

    return false

  }

  const denom = (y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1)

  if (denom === 0) {

    return false

  }

  const ua = ((x4 - x3) * (y1 - y3) - (y4 - y3) * (x1 - x3)) / denom
  const ub = ((x2 - x1) * (y1 - y3) - (y2 - y1) * (x1 - x3)) / denom

  return {
    x: x1 + ua * (x2 - x1),
    y: y1 + ua * (y2 - y1),
    seg1: ua >= 0 && ua <= 1,
    seg2: ub >= 0 && ub <= 1
  }

}


const swap = (arr, el1, el2) => {

  let swapedElem = arr[el1]
  arr[el1] = arr[el2]
  arr[el2] = swapedElem

}

const quickSortPure = (arr, leftPos = 0, rightPos, arrLength) => {

  // Thanks relu
  //https://stackoverflow.com/questions/38732480/native-javascript-sort-performing-slower-than-implemented-mergesort-and-quicksor
  if (rightPos === undefined) rightPos = arr.length - 1
  if (arrLength === undefined) arrLength = arr.length

  let initialLeftPos = leftPos
  let initialRightPos = rightPos
  let direction = true
  let pivot = rightPos
  while ((leftPos - rightPos) < 0) {

    if (direction) {

      if (arr[pivot] < arr[leftPos]) {

        swap(arr, pivot, leftPos)
        pivot = leftPos
        rightPos--
        direction = !direction

      } else
        leftPos++

    } else {

      if (arr[pivot] <= arr[rightPos]) {

        rightPos--

      } else {

        swap(arr, pivot, rightPos)
        leftPos++
        pivot = rightPos
        direction = !direction

      }

    }

  }
  if (pivot - 1 > initialLeftPos) {

    quickSortPure(arr, initialLeftPos, pivot - 1, arrLength)

  }
  if (pivot + 1 < initialRightPos) {

    quickSortPure(arr, pivot + 1, initialRightPos, arrLength)

  }

  return arr

}


const quickSortObj = (arr, key, leftPos = 0, rightPos, arrLength) => {

  // Thanks relu
  //https://stackoverflow.com/questions/38732480/native-javascript-sort-performing-slower-than-implemented-mergesort-and-quicksor

  if (rightPos === undefined) rightPos = arr.length - 1
  if (arrLength === undefined) arrLength = arr.length

  let initialLeftPos = leftPos
  let initialRightPos = rightPos
  let direction = true
  let pivot = rightPos
  while ((leftPos - rightPos) < 0) {

    if (direction) {

      if (arr[pivot][key] < arr[leftPos][key]) {

        swap(arr, pivot, leftPos)
        pivot = leftPos
        rightPos--
        direction = !direction

      } else
        leftPos++

    } else {

      if (arr[pivot][key] <= arr[rightPos][key]) {

        rightPos--

      } else {

        swap(arr, pivot, rightPos)
        leftPos++
        pivot = rightPos
        direction = !direction

      }

    }

  }
  if (pivot - 1 > initialLeftPos) {

    quickSortObj(arr, key, initialLeftPos, pivot - 1, arrLength)

  }
  if (pivot + 1 < initialRightPos) {

    quickSortObj(arr, key, pivot + 1, initialRightPos, arrLength)

  }
  return arr

}
