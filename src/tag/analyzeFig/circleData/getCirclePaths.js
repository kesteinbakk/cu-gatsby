

const me = 'getCirclePaths'

const getCirclePaths = (data, pathData) => {

  const {

    markFig: { mark },
    isHit,
    image,

  } = data


  const {

    debug,
    [me]: { slowInc, fastInc, },
    constants: { PIx2 },

  } = data.input


  const {


    angle = 0,
    numberOfPaths = 3,
    startAtSize = 1

  } = pathData

  const { log, myError, tearDown } = data.setup(me)


  const searchForEndPoint = (angle => {

    const calc = {
      currentSize: startAtSize,
      size: startAtSize,
      pos: false,
      counter: 0,
      final: false,
    }

    do {

      if (++calc.counter > 10000) {

        throw new Error('Neverending loop in circ size check')

      }

      const x = pathData.x + (calc.currentSize * Math.cos(angle))
      const y = pathData.y + (calc.currentSize * Math.sin(angle))


      if (debug) {

        if (x > image.size || y > image.size) {

          throw myError('x or y outside image', data, 'endPoints')

        }

        if (isNaN(x + y)) {

          throw myError('x or y is nan', data, 'endPoints')

        }


        mark({ x, y, type: 'paths' })

      }


      if (isHit({ x, y })) {

        calc.pos = { x, y }

        calc.size = calc.currentSize

        calc.currentSize += calc.final ? slowInc : fastInc

        if (calc.maxLength && calc.currentSize >= calc.maxLength) {

          break

        }

      } else {

        // No hit
        if (!calc.pos) {

          // No hit at all

          if (debug) {

            mark({ x, y, type: 'errors' })

          }

          log.error('Trying to calc size, but start level is not a hit', calc, data)

          calc.size = 0
          calc.pos = pathData
          calc.finished = true

        } else {

          if (calc.final) {

            calc.finished = true

          } else {

            // Go back and go slower
            calc.final = true
            calc.maxLength = calc.currentSize
            // Go back one step
            calc.currentSize = calc.currentSize - fastInc + slowInc

          }

        }

      }


    } while (!calc.finished)

    if (debug) {

      // Mark spot where size is set
      mark({ ...calc.pos, type: 'pathsLast' })

    }

    return {
      ...calc.pos,
      size: calc.size,
      angle,
    }

  })


  // Start of logic

  if (debug) {

    if (isNaN(numberOfPaths + angle)) {

      log.fatal('Angle or numberOfPaths', angle, numberOfPaths)
      throw new Error('Invalid inputs for circle paths. Angle or numberOfPaths is NAN')

    }

  }


  const angleInc = PIx2 / numberOfPaths

  const paths = []

  let checkAngle = angle

  for (let counter = 0; counter < numberOfPaths; counter++) {

    checkAngle += angleInc

    while (checkAngle >= PIx2) {

      checkAngle -= PIx2

    }
    //log.debug('Checking circle. Acc is', acc)
    const info = searchForEndPoint(checkAngle)

    info.index = counter

    paths.push(info)

  }

  if (debug) {

    log.debug('Paths are', paths)

  }

  tearDown()

  return paths

}

export default getCirclePaths