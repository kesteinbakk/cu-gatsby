

/*

 getCircleInfo  - javascript


 Returns x, y (center) & radius,

*/


import getCirclePaths from './getCirclePaths'

import * as Combi from 'js-combinatorics'


const me = 'getCircleData'


const getCircleData = (data, point) => {

  const {

    utils,
    markFig: { mark },
    isHit,

  } = data

  const {
    debug,

    [me]: {
      numberOfPaths,
      //  requiredNoOfPointsMatch,
      centerCheckThreshold
    },
    //  constants: { PIx2,/* PId2,*/ PId6 },


  } = data.input


  const {
    //    getAverage,
    //    getDistance,
    quickSort,
    getCenterFromPoints,
    //    getMedian,
    getDistance,
    match,
  } = utils


  const { log, myError, tearDown } = data.setup(me)


  const getCenterMatches = (cmb, allCenters = []) => {


    // Get 3 combinations
    const pointsCombo = cmb.next()


    if (!pointsCombo) {

      log.error('Ran out of combinations. Total data', allCenters)
      throw myError('No center matches')

    }

    // log.debug('Trying combination', pointsCombo)

    const newCenter = getCenterFromPoints(...pointsCombo)


    if (!newCenter || isNaN(newCenter.x) || isNaN(newCenter.y) || !isHit(newCenter)) {

      return getCenterMatches(cmb, allCenters)

    }

    const currentData = {
      center: newCenter,
      points: pointsCombo
    }

    const matches = [currentData]


    // Compare new center to the others
    const noOfMatches = allCenters.reduce((acc, data) => {

      //log.debug('Comparing ', center.x, 'vs', newCenter.x)
      if (
        match(data.center.x, newCenter.x, centerCheckThreshold) &&
        match(data.center.y, newCenter.y, centerCheckThreshold)
      ) {

        //    log.debug('Got match!')
        matches.push(data)

        acc++

      }
      return acc

    }, 0)


    if (noOfMatches < 3) {

      // Add new center to total data array
      allCenters.push(currentData)

      // Try again with another combination
      return getCenterMatches(cmb, allCenters)

    }

    // log.debug('Matches:', matches)
    return matches

  }


  const getAverageData = (input) => input.reduce((acc, data, index, src) => {

    acc.x += data.center.x
    acc.y += data.center.y
    acc.radius += (data.points.reduce((acc, point) =>
      acc += getDistance(point, data.center), 0) / data.points.length)

    if (index === src.length - 1) {

      return {
        x: acc.x / src.length,
        y: acc.y / src.length,
        radius: acc.radius / src.length
      }

    }
    return acc

  }, { x: 0, y: 0, radius: 0 })


  // Main logic

  const points = getCirclePaths(data, { ...point, numberOfPaths })

  quickSort(points, 'size')

  const cmb = new Combi.combination(points, 3)


  const matches = getCenterMatches(cmb)

  if (debug) {

    matches.forEach(data => mark({ ...data.center, type: 'matchedCenters' }))

  }


  // Get average data from the matched centers
  const center = getAverageData(matches)

  if (isNaN(center.x + center.y + center.radius)) {

    //Failed center calc.
    log.error('Failed center', center)
    throw myError('Center was NAN', data, me)

  }

  center.angle = point.angle

  if (debug) {

    log.debug('Got center', center)

    mark({ ...center, type: 'centers' })

  }


  tearDown()

  return center

}


export default getCircleData