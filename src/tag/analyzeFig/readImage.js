
// Analyze init

import { createCanvas, loadImage } from 'canvas'

const me = 'readImage'


const readImage = async (data) => {


  const {

    uri,
    debug,
    cropX = 0,
    cropY = 0,

    constants: { MAX_SIZE, MIN_SIZE },
    [me]: { sizeFactorLevel }
  } = data.input


  const { log, myError, tearDown } = data.setup(me)


  if (!uri || typeof uri !== 'string') {

    log.error('Invalid fig uri. Got:', uri)
    throw myError('Invalid fig uri.', data)

  }


  // Wait for fig image to load, then draw fig to the created canvas

  return await loadImage(uri).then(image => {

    if (debug) {

      log.debug('Fig loaded')

    }

    if (!image || !image.width) {

      throw myError('Did not manage to get image data', data)

    }


    const size = Math.min(image.width, image.height)


    if (size < MIN_SIZE || size > MAX_SIZE) {

      log.error('Size/min/max', size, MIN_SIZE, MAX_SIZE)
      throw myError('Invalid size. Size was ' + size)

    }

    //Create canvas
    const canvas = createCanvas(size, size)
    const ctx = canvas.getContext('2d')

    //Draw fig image on canvas
    ctx.drawImage(image, 0, 0, size, size);

    // Store image data
    data.image = {
      data: ctx.getImageData(cropX, cropY, size - cropX, size - cropY).data,
      size,

      //  toBlog: canvas.toBlob,
      setPixelColor: ({ color, x, y }) => {

        ctx.fillStyle = color
        ctx.fillRect(x, y, 1, 1)

      },
      canvas,
      sizeLevel: Math.ceil(size / sizeFactorLevel)
    }

    tearDown()

    return data

  }).catch(err => {

    log.error('Invalid fig uri)')
    throw myError(err, data)

  })

}

export default readImage