

const me = 'getNext'


const getNext = (data, circle) => {

  const {

    markFig: { mark },
    isHit,
    //    utils: { quickSort }

  } = data

  const {
    debug,

    [me]: {
      defInc,
      angleModFactor,
    },
    // utils: { getAngleFromTwoPoints },
    constants: { PIx2, PId2 },

    //  setInfo
  } = data.input


  const { myError, log, tearDown } = data.setup(me)


  const getNextPoint = ({ x, y, radius, angle, inc }) => {

    if (debug) {

      if (isNaN(x + y + angle + radius)) {

        log.error('x/y/angle/radius', x, y, angle, radius)
        throw myError('Invalid inputs in getPoint')

      }

    }

    const checkSize = radius + radius * (inc || defInc)

    const point = {
      x: x + (checkSize * Math.cos(angle)),
      y: y + (checkSize * Math.sin(angle)),

    }


    if (!isHit(point)) {

      if (debug) {

        mark({ ...point, type: 'nextNoHit' })

      }
      return false

    }

    if (debug) {

      mark({ ...point, type: 'next' })

    }

    return point

  }


  const calcNext = (center, calc) => {


    if (!calc) {

      calc = Object.seal({
        angle: center.angle ? center.angle - PId2 : 0,
        data: null,
        maxAngle: center.angle ? center.angle + PId2 : false,
        i: 0,
        //    hit: null
      }) //calc

    } // ! points

    const { data, maxAngle, angle, i } = calc

    const currentAngle = angle + (i * angleModFactor)


    if (maxAngle && currentAngle >= maxAngle) {

      // We're finished
      return data

    }


    const point = getNextPoint({
      ...center,
      angle: currentAngle
    })

    calc.i++

    if (debug && calc.i > 50) {

      throw myError('Neverending loop in calcNext')

    }


    if (!point) {

      // Point not whithin a circle

      if (!data) {

        calc.data = [[]]

        if (!calc.maxAngle) {

          calc.maxAngle = currentAngle + PIx2

        }

      } else if (data[data.length - 1].length) {

        // create new dataSet
        data.push([])
        log.debug('Creating new dataset on angle', currentAngle, data)


      }

      // Continue
      return calcNext(circle, calc)

    }


    // Angle are within a circle - got hit

    if (!data) {

      if (calc.maxAngle) {

        throw myError('First next test was a hit. Start angle given by input')

      }

      // Move on to first no-hit to ensure all hits are stored together if no start angle
      return calcNext(circle, calc)

    }

    data[data.length - 1].push({ ...point, angle: currentAngle })

    // log.debug('hit', data)


    // loop
    return calcNext(center, calc)

  }


  // Main

  if (isNaN(circle.x + circle.y + circle.radius)) {

    log.error('Got circle', circle)
    throw myError('Invalid circle for getNext. Missing x, y or radius')

  }

  const dataSets = calcNext(circle)

  if (!dataSets || !dataSets.length) {

    throw myError('No data from find next')

  }


  // Remove last set if empty
  if (!dataSets[dataSets.length - 1].length) {

    dataSets.pop()

  }

  log.debug('Got data sets', dataSets)

  tearDown()

  return dataSets

}

export default getNext


/*const testForCenter = (current, prev) => {


  const centerCheckLength = current.radius * radiusIncFactorCenterCheck

  const angle = getAngleFromTwoPoints(current, prev)

  let i = 0

  const centerCons = []

  do {

    const thisAngle = angle + (i * PId3)

    const point = {
      x : current.x + ( centerCheckLength * Math.cos(thisAngle) ),
      y : current.y + ( centerCheckLength * Math.sin(thisAngle) ),
    }

    if ( ! isHit( point ) ) {
      if ( debug ) {
        mark({...point, type:'errorInfo'})
      }
      return false
    }

    if ( debug ) {
      mark({...point, type:'next'})
    }

    centerCons.push({
      ...point,
      angle: thisAngle
    })

  } while ( ++i < 6 )

  return centerCons
}
*/
