import { LoggerApi, LoggerOptions } from './logger'
import FigError from './FigError'
import * as markFig from './markFig'
import * as CONSTANTS from './constants'

/// Types for phases (file ids):

export type Phases =
  'getFigKey' | 'getFigData' | 'createFig' | 'draw' // Fig phases

/// Types for figure logic:

export type KeyMeta = [number, number, number, number, number, number] // Length of array must equal constant KEY_META_LENGTH
export type HalfLegFactors = [number, number, number, number, number, number]
export type LegFactors = [
  number, number, number, number, number, number, // Inner leg
  number, //king
  number, number, number, number, number, number // Outer leg
]
export type FigFactors = [LegFactors, LegFactors, LegFactors, LegFactors, LegFactors, LegFactors]

export type BaseFactors = {
  inner: HalfLegFactors,
  king: number,
  outer: HalfLegFactors
}
export type KeyBase = 10 | 16 | 36 | 46

/// Types for settings/config: (figSettings, debugSettings and logSettings)

export type DefaultFigSettings = {
  canvas?: string | HTMLCanvasElement | null,
  key: string,
  colorKey?: number[],
  size: number,
  light: string,
  satur: string,
  angle: number,
  color: number | string,
  centerSize: number,
  padding: number
  border?: number,
}

export type FigSettings = Partial<DefaultFigSettings>

export type DefaultDebugSettings = {
  clearConsole: boolean,
  logInfo: boolean,
  doChecks: boolean,
  noOfLegs?: 1 | 2 | 3 | 4 | 5 | 6,
  baseFactors?: BaseFactors,
  stopAfter?: number,
  freeze?: object,
  //maxTicks?: number,
  fakeCenter?: object | boolean,
  kingControlThreshold: number,
  drawControlKings: boolean,
  markBigCenter: boolean,
  drawDebugInfo: boolean,
  circles: {
    light: string,
    satur: string,
    color: number | string
  },
  textColor: string,
  logSettings: LoggerOptions & {
    expandLogGroup: { [key in Phases]: boolean }
  }
}

export type DebugSettings = Partial<DefaultDebugSettings>

export type Settings = {
  figSettings: Partial<DefaultFigSettings>,
  debugSettings?: Partial<DefaultDebugSettings>
}

export type Input = { figSettings: DefaultFigSettings, debugSettings: DefaultDebugSettings }


/// Types for support functions
export type Match = (num1: number, num2: number, threshold?: number, base?: number) => boolean | number

type PipeInputFunction = (data: PipeData) => PipeData
type PipeStartFunction = (x: PipeData) => PipeData
export type Pipe = (...fns: PipeInputFunction[]) => PipeStartFunction


type Setup = (id: Phases, expand?: boolean) => {
  log: LoggerApi
  tearDown: () => void,
  myError: (msg: string) => FigError,
  stop: () => never
}


/// Types for data:

export type Config = {
  figSettings: DefaultFigSettings & { key: string },
  debugSettings: DefaultDebugSettings,
  CONSTANTS: typeof CONSTANTS
}

export type CircleData = {
  x: number,
  y: number,
  radius: number,
  debug?: boolean
}

export type CircleDataDebug = {
  no: number,
  x: number,
  y: number,
  radius: number,
  R: number,
  angle: number
}

type FigData = {
  factors?: FigFactors
  circles: CircleData[],
  /*  angle: number,
   center: CircleData,
   bigSize: number,
   centerSize: number, */
  //  keyMeta: KeyMeta,
}

type KeyData = {
  figKey: string[], // The key to be used when generating the fig as an array
  keyBase: KeyBase // the number of elements the key consists of (see constans key bases)
  figBase: number // the base we need to use in the fig (the number of different sizes or colors) to contain the key
}

export type DebugData = {
  circles: CircleDataDebug[],
  kings: { x: number, y: number, radius: number, factor: number }[],
  last: { x: number, y: number, angle: number }[],
  controlKings: CircleData[],
  steps: number
}

export type Info = { [key: string]: any }

export type OutputData = {
  getStatus(): string | string[],
  getInfo(): Info,
  getTicks(): number,
  time: {
    start: number,
    end: number | null,
    used: string
  }
  key?: string
  coreKey?: string
  uri?: string,
}

export type PipeData = {
  config: Config,
  canvas: HTMLCanvasElement,
  match: Match
  setup: Setup,
  setStatus: (str: string) => void,
  setInfo: (obj: Info | string, val?: any) => void
  markFig: typeof markFig,
  keyData?: KeyData, //Added in writeFigkey
  figData?: FigData, // Added by create/init
  debugData?: DebugData, // Will only be added in pipe if debug is true
  output: OutputData
}


/// Main fig api


type CallbackOnDraw = (data: OutputData) => void

export type MyApi = {
  draw: (figSettings: FigSettings, debugSettings?: DebugSettings) => PipeData['output'],
  onDraw: (func: boolean | CallbackOnDraw, keepOld: boolean) => void
  isDrawn: boolean,
  drawData: PipeData | null,
}
