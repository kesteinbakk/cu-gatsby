import { BaseFactors } from './figTypes'
import { KeyBase } from './keyTypes'


const PI = Math.PI

const BASE_FACTORS: BaseFactors = {
  inner: [0.4, 0.5, 0.6, 0.7, 0.85, 0.95],//[0.4, 0.5, 0.6, 0.7, 0.85, 0.95],
  king: 1.0,
  outer: [0.95, 0.85, 0.7, 0.6, 0.5, 0.4],
}

const KEY_BASES: KeyBase[] = [
  10, // Int
  16, // Hex
  36, // //a-z and 0-9
  46 // withSpecialChars:
]

const SUPPORTED_CHARS: { [key: string]: string } = {
  ':': '<36>',
  ';': '<37>',
  '{': '<38>',
  '}': '<39>',
  '"': '<40>',
  '!': '<41>',
  ',': '<42>',
  '&': '<43>',
  ' ': '<44>',
}


export const FIG_CONSTANTS = {

  FIG_VERSION: 1,

  PI,
  PId3: PI / 3,
  MY_CONSTANT: PI / 15,

  MAX_SIZE: 10000,
  MIN_SIZE: 25,

  LEGS: 6,
  LEG_SUM: 9,
  LEG_LENGTH: 13,
  FIG_LENGTH: 78, // Not counting center

  /*   PI,
    PIx2: PI * 2,
    PId2: PI / 2,
    PId4: PI / 4,
    PId5: PI / 5,
    PId6: PI / 6, */

  MAX_FIG_BASE: 7,
  MIN_FIG_BASE: 3, //For base 4: 333 = 63 , 33=15

  BASE_FACTORS,
}


export const KEY_CONSTANTS = {
  KEY_LENGTH: 60, // V.1 No center, no queens (12), No kings/meta,
  KEY_META_LENGTH: 6,
  FACTOR_AREA: 0.1,
  KEY_BASES,
  SUPPORTED_CHARS
}