import { RadianFactors, FigSettings } from '../figTypes'
import { createDrawCircleFunc } from './utils';



const draw = (factors: RadianFactors, settings: FigSettings): void => {

  const { size, canvas, angle = 0, useRandomColors, color, fill, colorKey } = settings

  // Prepare canvas
  canvas.width = canvas.height = size;
  const ctx = canvas.getContext('2d');

  // Calculate pi divided by 3. Since we are going to create 6 legs within a circle, and a compelte circle is 2PI, each leg is 2PI/6=PI/3 radians apart.
  const PId3 = Math.PI / 3;

  // Calculate sizes for the figure. We could have used ctx.canvas.clientWidth instead of passing in the size, but passing the size seemed more intuitive for readers

  const bigCircleRadius = size / 5; // This is the size of the "invisible" big circle which curve the smaller circles follows

  // Calculate center of image (just by deviding the image size by 2)
  const origo = Math.round(size / 2);

  const stopAfterLeg = 6; // We can either keep track of the leg number or stop when the angle have completed a full circle (2*PI)

  // Get a function that draws a circle on the canvas
  const drawCirc = createDrawCircleFunc({ ctx, fill, useRandomColors, defColor: color });

  // Draw center circle
  const centerSize = bigCircleRadius * factors.center;
  drawCirc(origo, origo, centerSize);

  // Start drawing legs
  createLeg(angle);

  function createLeg(angle: number, leg = 1) {


    const legCenterX = origo + (bigCircleRadius * Math.cos(angle));
    const legCenterY = origo + (bigCircleRadius * Math.sin(angle));

    // Create start angle
    // PI inverts leg center angle compared to the starting angle
    // We then move the angle the length of the center circle which we which we have already drawn.
    const legAngle = angle - Math.PI + factors.center;

    // Create a copy of the leg factors for this leg. Use the first if not provided for this leg
    const legFactors = [...factors.legs[leg - 1]];

    //console.debug('Creating leg.', 'Angle', angle, 'Leg:', leg, 'Factors', legFactors);

    // Draw circles in this leg
    createCircles({ legCenterX, legCenterY, legAngle, legFactors });

    // Uncommenting the next line will draw the six big hidden circles which path the smaller ones follow
    // drawCirc(legX, legY, bigCircleRadius, 'green', false);

    // Check if we are finished
    if (++leg > stopAfterLeg) {
      return
    }

    // Move to next leg
    angle += PId3;
    // Create a copy of the factor array for the next leg (or use the current one if no factors are provided for this leg)
    const nextLegFactors = factors.legs[leg] || legFactors;
    // Create next leg
    return createLeg(angle, leg)
  }


  // Function for creating circles in a leg following the path of what we call "the big circle"
  type CreateCircleProps = {
    legCenterX: number,
    legCenterY: number,
    legAngle: number,
    legFactors: number[],
  }
  type CreateCircleFunc = (data: CreateCircleProps) => CreateCircleFunc | void

  function createCircles(data: CreateCircleProps): CreateCircleFunc | void {

    const { legCenterX, legCenterY, legFactors } = data;
    let { legAngle } = data;

    // Get the factor for current circle
    const factor = legFactors.shift();

    // Check if factor array was empty - if so we're finished with this leg
    if (!factor) {
      return;
    }

    // Now starts the interesting part. Keep up... 
    // Note the connection between the angle within the "big circle" to the size of the small circles.
    // We move along the arc of the big circle and at the same time we know the radius of the small circles

    // Calculate the size of the small circle
    const radius = factor * bigCircleRadius;

    // Increase the angle of the big circle equal to the factor of the current small circle
    legAngle += factor;
    // Now curve is at center of the current small circle

    // Calculate coordinates along the big circle curve at this angle 
    const x = legCenterX + bigCircleRadius * Math.cos(legAngle);
    const y = legCenterY + bigCircleRadius * Math.sin(legAngle);

    /// Then draw the circle
    drawCirc(x, y, radius);


    // Push the angle further ahead equal the radius of the current small circle,
    legAngle += factor;
    // Now curve is at end of the circle

    // Start over with next circle
    return createCircles({ legCenterX, legCenterY, legAngle, legFactors });

  }


  /*   // Function to draw a circle on the canvas
    function drawCirc(x: number, y: number, r: number, color = 'red', fillCircle = true) {
      ctx.beginPath();
      ctx.fillStyle = color;
      ctx.arc(x, y, r, 0, 2 * Math.PI);
      if (fillCircle) {
        // Fill circle with color
        ctx.fill();
      } else {
        // Just draw outline
        ctx.stroke();
      }
  
    } */

};

export default draw;