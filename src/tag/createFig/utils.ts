import { MyFactors, RadianFactors } from '../figTypes'
import { FIG_CONSTANTS } from '../constants'

const { LEG_LENGTH, LEGS } = FIG_CONSTANTS
// Time tracking functions
export const myTimer = (initName: string) => {

  type MyTimer = {
    startTime: number,
    name: string,
    start: (name?: string) => void,
    stop: (name?: string) => void
  }

  const myTimer: MyTimer = {
    startTime: 0,
    name: initName || 'noName',
    start: (name) => {
      myTimer.name = name || myTimer.name
      myTimer.startTime = Date.now()
      console.log('Starting', myTimer.name)
    },
    stop: (endMsg) => {
      console.log('Ending', myTimer.name, 'Used', Date.now() - myTimer.startTime, 'ms.', endMsg || '')
    }
  }
  return myTimer

}



// Matches two numbers. Returns true if exact match, the difference if within
// the threshold, and false if outside the threshold
export const match = (num1: number, num2: number, threshold?: number, base?: number) => {

  if (num1 === num2) { return true }

  threshold = threshold || 0.00001
  base = base || Math.max(num1, num2)

  const offset = threshold * base

  if (num1 < (num2 + offset) && num1 > (num2 - offset)) {
    return Math.abs(num2 - num1)
  } else {
    return false
  }

}

// Get a random integer number between min and max
export const randomInt = (max: number): number => {
  return Math.floor(Math.random() * max)
}

// Get a random number between min and max
export const random = (min: number, max: number): number => {
  return min + Math.random() * (max - min);
}


// Converts from MyFactors to radian factors by multiplying with PI/15
export const convertToRadianFactors = (factors: MyFactors): RadianFactors => {
  const k = Math.PI / 15;

  // We multiply the factor number with k.
  // Multiplying with k is just for convenience to let us use factor numbers which are simpler to work with. 

  return {
    center: factors.center * k,
    legs: factors.legs.map((legFactors) => legFactors.map(v => v * k))
  }
}

// Cheks that the sum of inner factors and the sum of outer factors together with center and king is 20.
export const checkMyFactors = (factors: MyFactors) => {

  // Check if we have the correct number of factor legs
  if (factors.legs.length != LEGS) {
    console.error('Invalid factors', factors);
    throw Error('Invalid factor array length. Not corresponding to number of legs');
  }

  // Check if we have a numeric value for center
  if (typeof factors.center != 'number') {
    console.error('Invalid factors', factors);
    throw Error('Invalid center in factors. Not a number');
  }

  // Since the array contains the radius of the circles, we need to multiply them with 2 to get the complete size/length of the circle
  factors.legs.forEach((legFactors, index) => {

    // Check number of factors in this leg
    /*     if (legFactors.length != LEG_LENGTH) {
          console.error('Invalid factors', factors);
          throw Error('Invalid number of factors in leg ' + index);
        } */
    legFactors.forEach(num => {
      if (isNaN(num) || num < 0) {
        console.error('Invalid factor item. Not a number or negative number', legFactors);
        throw Error('Factors contain invalid values')
      }
    })
    const kingIndex = getKingIndex(legFactors)
    const sumWithoutNextKing = legFactors.reduce((prev, cur) => prev + cur, 0) * 2 + factors.center;
    const nextKing = index < 5 ? factors.legs[index + 1][kingIndex] : factors.legs[0][kingIndex];
    const sumWithNextKing = round(sumWithoutNextKing + nextKing);
    // When we create random factors we subtract the current value from 10 to find the last value in the leg array. 
    // Logically this should always lead to a sum of 10: x + (10-x) = 10
    // Due to javascripts logic adding the difference between 10 and a floating number to the same floating number may lead to results like 9.999999999 instead of 10 
    // thus we need roundt the values before the test.

    if (sumWithNextKing !== 20) {
      const msg = `Invalid factors! Sum for leg factors in index ${index} was: ${sumWithNextKing}. It should have been 20!`;
      console.error(msg);
      console.debug(factors.legs[index]);
      throw Error(msg + ' See console for factor object.');
    }
  });

  // Function for detecting which circle is king. That is which circle has its center at legSize 10
  function getKingIndex(arr: number[]) {
    let size = factors.center;
    return arr.findIndex((el) => {
      size += el * 2;
      if (size > 10) return true;
    })
  }
}

// The factors in a leg must sum up to 4 to create figure
export const checkHalfLegFactors = (data: number[]) => {

  const sum = data.reduce((all, cur) => all + cur)

  if (!match(sum, 4)) {
    console.error('Invalid half leg factors', data)
    throw Error('Invalid half leg factors. Sum was not 4. Sum was' + sum)
  }

}

// Function to draw a circle on the canvas
type CreateDrawCircleParameters = {
  ctx: CanvasRenderingContext2D,
  useRandomColors?: boolean,
  defColor?: string,
  fill?: boolean
}

export const createDrawCircleFunc = (args: CreateDrawCircleParameters) =>
  (x: number, y: number, r: number, color?: string): void => {
    const { ctx, useRandomColors = false, defColor = 'red', fill = true } = args;

    ctx.beginPath();
    ctx.fillStyle = useRandomColors ? getRandomColor() : color || defColor;
    ctx.arc(x, y, r, 0, 2 * Math.PI);
    if (fill) {
      // Fill circle with color
      ctx.fill();
    } else {
      // Just draw outline
      ctx.stroke()
    }


  }

function getRandomColor(): string {
  const letters = '0123456789ABCDEF';
  let color = '#';
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

// Round a number to prevent js floating number error
function round(num: number) {
  return Math.round(num * 100000) / 100000
}
