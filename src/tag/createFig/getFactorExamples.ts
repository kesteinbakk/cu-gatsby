// We have 6 "spokes" or "legs" of smaller circles going from the center and tangents the next leg.
// The circle in the center of the leg, connecting the outer leg and the inner leg, we call the "king" circle for easy referance.
// In this example all 6 legs are equal, but this does not need to be the case.
// The arc of each leg follows a bigger "invisible circle" we call the "big circle".
// Each number below in the "factors" represent the size of the small visible circles.

// We have here chosen to mainly have 6 circles on each side of the king circle, but this does not need to be the case

// The small circles are laid out on 2/3 of the complete curve of the large invisible circle. 
// A full circle round corresponds to 2PI in radians, so 2/3 rounds equals 4/3*Pi or 1,3333Pi. 
// But in order for us to more easily set circle sizes with more intuitive numbers, we use a constant in our logic that multiply our factors with PI/15
// This means the 2/3 of the big circle corresponds to 20 of our factors (4/3 = 20/15)

// We divide each leg in an "inner" and an "outer" part. Separated by the king.

// The total length of the inner leg is the radius of the center, all diameters of the circles in the inner leg, and the radius of the king
// The total length of the outer leg is the radius of the king, all diameters of the circles in the outer leg, and the radius of the king in the next leg in the figure
// In this excample all legs are equal, so we can use the same radius for both kings, but in another implementation I have made logic with different sized kings.

// The total sum of all circles from the center of the figure to the center of the king (along the inner leg), therefore needs to be 10
// The total sum of all the circles from the king's center to the center of the king in the next leg, also needs to be 10. 

import { random, checkMyFactors, convertToRadianFactors } from './utils';
import { MyFactors, RadianFactors } from '../figTypes';



function getFactorExamples(type: string): RadianFactors {


  const legSize = 20;
  const halfLegSize = legSize / 2;

  const getSumFunc = (arr: number[], add = 0) => () => (arr.reduce((v, i) => v + i, 0) * 2) + add

  const myFactors = selectFactorsBasedOnType();

  // Copy first leg if fewer provided
  while (myFactors.legs.length < 6) {
    myFactors.legs.push(myFactors.legs[0]);
  }

  console.info('Using my factors', myFactors);

  checkMyFactors(myFactors);

  const factors: RadianFactors = convertToRadianFactors(myFactors);

  //console.debug('Factors converted are', factors);

  return factors;




  function selectFactorsBasedOnType() {
    switch (type) {
      case 'normal': return getBase();
      case 'random': return getRandom();
      case 'small': return getMany();
      default: throw Error('Missing implementation of factors for type' + type);
    }
  }


  function getBase(): MyFactors {

    // Just to ease reading
    const center = 1.0 // The center circle
    const inner = [0.4, 0.5, 0.6, 0.7, 0.85, 0.95] //From center and outwards. The sum here is 4.
    const king = 1.0 // The "connection circle" in the middle of each "leg"
    const outer = [0.95, 0.85, 0.7, 0.6, 0.5, 0.4] // From the "connection circle" towards the next "connection circle". The sum here is also 4


    return {
      center,
      legs: [
        [...inner, king, ...outer] // We only return one leg, as all legs will be equal
      ]
    }
  }


  function getAlt1(): MyFactors {

    return {
      center: 0.6, // Center below 1
      legs: [
        [
          0.3, 0.8, 0.3, 0.7, 0.4, 1.3, // The sum of the radii here is 3,8. Thus, the total length of all the circles (their diameters) are 7.6 And adding the center gives ut 8.2. The king circle therefore needs to be 1.8 in order to reach 10
          1.8, // King above 1
          0.4, 0.7, 0.45, 0.45, 0.4, 0.8 // Since the king circle of both the current and the next leg are 1.8, we have already used 3.6 of the distance. The rest of the circles in the outer leg needst to be 6.4 (3.6 + 6.4 = 10). Their radii needs to sum up to 3.2. Which this array does.
        ]
      ]
    }
  }

  function getRandom(): MyFactors {
    // We here generate random sizes within a given interval


    const center = random(0, 2);
    const king = random(0, 2);
    const inner = createRandomHalfLeg(center + king);
    const outer = createRandomHalfLeg(king * 2);

    //  console.debug('CHECK', center + king + inner.reduce((v, i) => v + i, 0) * 2)
    //  console.debug('CHECK', king * 2 + outer.reduce((v, i) => v + i, 0) * 2)

    return {
      center: center,
      legs: [
        [...inner, king, ...outer]
      ]
    }

    function createRandomHalfLeg(dif: number) {
      const arr = [];

      const halfLegSum = getSumFunc(arr, dif)

      do {
        let num = random(0.1, 1.3)
        arr.push(num);
      } while (halfLegSum() < halfLegSize);

      // Remove item that exceeded halfLegSize 
      arr.pop();
      // Get diamenter of remaining size
      const dia = halfLegSize - halfLegSum()
      // Add radius to array
      arr.push(dia / 2);
      //console.debug("Finished with sum", halfLegSum())
      return arr;
    }
  }

  // This excamples illustrates a different number of circles
  function getMany(): MyFactors {

    const center = 0;
    const arr = [];

    const getInnerLegSize = getSumFunc(arr)

    let i = 0.01;
    const inc = 0.008;
    let sum: number;
    do {
      arr.push(i);
      i += inc;
    } while (getInnerLegSize() + 2 * i + inc < halfLegSize) // We need to stop before king circle

    const king = halfLegSize - getInnerLegSize()

    if (king < 0) throw Error("king below 0")

    // Add king and reset inc to last pushed fig
    arr.push(king);
    i -= inc;

    const getFullLegSize = getSumFunc(arr, king) // We add king as the leg intersect with next king

    do {
      arr.push(i);
      i -= inc;
      if (i <= 0) throw Error('I less than 0');
    } while (getFullLegSize() + i < legSize)

    const dia = legSize - getFullLegSize()

    arr.push(dia / 2);
    return {
      center: center,
      legs: [
        [...arr],
      ]
    };
  }
}


export default getFactorExamples