
// Here we convert a string or a number to fig factors representing the given data


import { MyFactors, RadianFactors } from '../figTypes';
import { KeyData, KeyBase } from '../keyTypes';
import { FIG_CONSTANTS, KEY_CONSTANTS } from '../constants';
import { checkMyFactors, checkHalfLegFactors, convertToRadianFactors } from './utils';
import getFigKey from '../key/getFigKey'

const {
  BASE_FACTORS,
  LEGS
} = FIG_CONSTANTS

const {
  KEY_BASES,
  FACTOR_AREA
} = KEY_CONSTANTS;



// Main func
const getDataFactors = (key: string): RadianFactors => {

  const keyData = getFigKey(key);

  if (!keyData) { throw Error('Missing keydata for calculating factors') }

  const {
    figKey,
    keyBase,
    figBase,
  } = keyData



  if (!figKey) {
    throw Error('Missing figKey for fig gen. Typeof figKey was ' + typeof figKey)
  }

  const step = FACTOR_AREA / (2 * (figBase - 1))  // Half for key inc and half for next dec


  // console.debug('Factor step is', step, '(Area available was', FACTOR_AREA, ')')


  // The numeric values from the figKey is multiplied with a factor based on the
  // numeric base chosen for displaying the value in the fig
  const incKey = figKey.map(item => +item * step)

  const meta = getMeta(step, keyBase)

  //data.figData.keyMeta = meta

  const myFactors: MyFactors = {
    center: 1, // Todo: Handle different center sizes?
    legs: getFactors({ meta, incKey }) as number[][]
  }


  if (incKey.length) {
    throw Error('I have leftovers of key (' + incKey.length + ')!')
  }

  checkMyFactors(myFactors);


  return convertToRadianFactors(myFactors);
}


const getMeta = (step: number, keyBase: KeyBase) => {

  // Length of array must equal KEY_META_LENGTH
  const keyMeta = [
    FACTOR_AREA, // Max offset. Start king. This number should be one above all the rest.
    step,
    KEY_BASES.indexOf(keyBase) * step,//kb[0], // keyBaseIndex1
    0, //Use color
    0, // Available
    0, // Available
  ]

  //console.debug('META IS', keyMeta)

  return keyMeta

}


type GetLegPartFunc = (bases: number[], incKey: number[]) => number[]

const getLegPart: GetLegPartFunc = (bases, incKey) => {

  let offset = 0

  return bases.map((base, index) => {

    // Return factors
    if (index === bases.length - 1) {

      // No value on last circle. We use this circle to clear offset for kings
      return base - offset

    }

    //Key value
    const inc = incKey.shift()
    //console.debug('Inc for index', index, 'is', inc)


    // Circle size equals the base + keyInc - last offset
    const val = base + inc! - offset

    // Set new offset
    offset = inc!

    return val

  })


}


type GetFactorsArgs = { factors?: number[][], meta: number[], incKey: number[] }
type GetFactorsFunc = (args: GetFactorsArgs) => GetFactorsFunc | number[][]

const getFactors: GetFactorsFunc = ({ factors = [], meta, incKey }) => {

  const innerLeg = getLegPart(BASE_FACTORS.inner, incKey)
  const outerLeg = getLegPart(BASE_FACTORS.outer, incKey)

  checkHalfLegFactors(innerLeg)
  checkHalfLegFactors(outerLeg)


  const kingInc = meta[factors.length]
  const halfKingInc = kingInc / 2 // Radius increase
  const nextIndex = factors.length + 1 !== meta.length ? factors.length + 1 : 0
  const nextKingInc = meta[nextIndex]
  const halfNextKingInc = nextKingInc / 2

  const kingFactor = BASE_FACTORS.king + kingInc
  innerLeg[5] -= (halfKingInc)
  outerLeg[5] -= (halfKingInc + halfNextKingInc)

  const legFactors = [
    ...innerLeg,
    kingFactor,
    ...outerLeg
  ]


  factors.push(legFactors)

  // If we haven't created all 6 legs, do it again
  if (factors.length < LEGS) {
    return getFactors({ factors, meta, incKey })
  }

  // Else return factors
  return factors
}


export default getDataFactors