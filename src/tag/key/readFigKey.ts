import bigInt from 'big-integer'

import { PipeData } from '../figTypes'

const me = 'readFigKey'


const readFigKey = (data: PipeData) => {

  const {
    config: {
      debugSettings: { logInfo },
      CONSTANTS: { KEY_CONSTANTS: { KEY_BASES, KEY_LENGTH, SUPPORTED_CHARS }
      }
    },
    keyData
  } = data

  const { myError, log } = data.setup(me, false)

  if (!keyData) {
    throw myError('No keydata in data provided for reading key')
  }

  const {
    figKey,
    figBase,
    keyBase,
  } = keyData


  if (!figKey || !Array.isArray(figKey)) {
    throw myError(`Missing or invalid fig key. FigKey was ${JSON.stringify(figKey)}`)
  }

  if (figKey.length !== KEY_LENGTH) {
    throw myError('Invalid fig key length. Got figKey length: ' + figKey.length)
  }

  if (!figBase || isNaN(figBase + keyBase)) {
    log.error('FigBase/keyBase', figBase, keyBase)
    throw myError('Invalid figBase and/or keyBase')
  }

  // Replace non alphabetic characters with code from settings
  const handleSpecialChars = (key: string) => Object.entries(SUPPORTED_CHARS).reduce((string, [key, val]) => {
    return string.split(val).join(key)
  }, key)


  // Get key
  const figKeyString = figKey.join('') // Still padded

  // Convert back to plain text
  const coreKey = bigInt(figKeyString, figBase).toString(keyBase)

  if (!coreKey) {
    throw myError('Invalid core key')
  }

  if (logInfo) {
    log.debug('CoreKey is', coreKey)
  }

  // Check if special chars are included in key
  data.output.key = (keyBase > KEY_BASES[2]) ?
    handleSpecialChars(coreKey) : coreKey


  data.output.coreKey = coreKey

  return data

}

export default readFigKey