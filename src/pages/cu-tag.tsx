import React from 'react'
import { Tabs, Tab, Typography, Box, Container, Stack, Paper } from '@mui/material';

import CuTagDraw from '../components/cu-tag-draw';
import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';


/* const modalStyles: Modal.Styles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
  },
} */




type Props = {
  tagKey: String
  useModal?: boolean
}

const CuTag = ({ tagKey = 'test', useModal = false }: Props) => {

  type TabPanelProps = {
    children?: React.ReactNode;
    index: number;
    value: number;
  }


  //const [modalIsOpen, setIsOpen] = React.useState(useModal ? true : false)

  const [activeTab, setActiveTab] = React.useState(0);




  /*   function openModal() {
      setIsOpen(true)
    } */



  /*   function afterOpenModal() {
      // reference to canvas should now be accessible
      //if (subtitle?.style) { subtitle.style.color = '#f00' };
      figSettings.canvas = canvas
      fig.draw(figSettings, debugSettings)
      // console.log('Canvas is', canvas)
    }
  
    function closeModal() {
      setIsOpen(false)
    } */



  const MyTabs = () => {

    const handleTabChange = (event: React.SyntheticEvent, newValue: number) => {
      setActiveTab(newValue);
    };

    function TabPanel(props: TabPanelProps) {
      const { children, value, index, ...other } = props;

      return (
        <div
          role="tabpanel"
          hidden={value !== index}
          id={`simple-tabpanel-${index}`}
          aria-labelledby={`simple-tab-${index}`}
          {...other}
        >
          {value === index && (
            <Box sx={{ p: 3 }}>
              <Typography>{children}</Typography>
            </Box>
          )}
        </div>
      );
    }

    function a11yProps(index: number) {
      return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
      };
    }

    return (
      <React.Fragment>
        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
          <Tabs value={activeTab} onChange={handleTabChange} aria-label="basic tabs example">
            <Tab label="Item One" {...a11yProps(0)} />
            <Tab label="Item Two" {...a11yProps(1)} />
            <Tab label="Item Three" {...a11yProps(2)} />
          </Tabs>
        </Box>
        <TabPanel value={activeTab} index={0}>
          Item One
        </TabPanel>
        <TabPanel value={activeTab} index={1}>
          Item Two
        </TabPanel>
        <TabPanel value={activeTab} index={2}>
          Item Three
        </TabPanel>
      </React.Fragment>
    )
  }

  /* if (useModal) {
      return (
       <div>
         <button onClick={openModal}>Open Modal</button>
         {/*         <Modal
           isOpen={modalIsOpen}
           onAfterOpen={afterOpenModal}
           onRequestClose={closeModal}
           style={modalStyles}
           contentLabel="MeVeM Fig Tag"
         >
 
           <canvas ref={ref => (canvas = ref)} />
         </Modal> }
       </div>
     ) */

  //const drawerWidth = 250;

  return (
    <Paper>
      <CuTagDraw />
    </Paper>
  )

}

export default CuTag
